package com.example.dhwanitrivedi.audiotest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import ai.api.AIConfiguration;
import ai.api.AIListener;
import ai.api.AIService;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Result;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements AIListener{

    private Button Show_Weather;
    private AIService aiService;

    private static String url="http://dhwani-test.apigee.net/v1/weatherapikey/forecastrss?w=12797282&apikey=0qzOmQJs0UNMMpLq2mHQWKFAW5Wvv1jP";
    private static String mRequest = "{\"city\" : \"san fransisco\", \"country\" : \"United States of America\", \"Zipcode\" : \"95051\"}";
    private static String TAG = "audioTest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        TextView t = (TextView)findViewById(R.id.request_content);
        t.setText(mRequest);


        Log.d(TAG, "Initializing Api.ai SDK now");
        final AIConfiguration config = new AIConfiguration("1dcd69f9414d4067be0026230c5ccc55",
                "793d52a3-6313-4943-83c0-1ff0c1b2d1a5 ", AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
        aiService = AIService.getService(this, config);
        aiService.setListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onShow_Weather(View view) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.POST, url, mRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // the response is already constructed as a JSONObject
                        Log.d(TAG, response.toString());

                        TextView t = (TextView)findViewById(R.id.response_content);
                        t.setText(response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(this).add(jsonRequest);
    }


    @Override
    public void onResult(AIResponse aiResponse) {
        Log.d(TAG, "Result Received");
        final Result result = aiResponse.getResult();
        Log.d(TAG, result.getResolvedQuery());
        if(result.getResolvedQuery().equalsIgnoreCase("refresh")) {
            refresh();
        }
    }

    @Override
    public void onError(AIError aiError) {

    }

    @Override
    public void onAudioLevel(float v) {

    }

    @Override
    public void onListeningStarted() {
        Log.d(TAG, "Listening Started");
    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {
        Log.d(TAG, "Listening Finished");
    }


    /////


    public void onRefresh(View view) {
        Log.d(TAG, " refresh button clicked");
        refresh();
    }

    public void onListen(View view) {
        Log.d(TAG, " listen button clicked");
        aiService.startListening();
    }

    public void onStopListen(View view) {
        Log.d(TAG, " stop button clicked");
        aiService.stopListening();
    }


    private void refresh() {

        Show_Weather.setEnabled(false);

    }


}
